const express = require("express")
const User = require("../models/userModels")
const Product = require("../models/productModel")
const Order = require("../models/Order")
const bcrypt = require("bcrypt")
const auth = require("../auth")



module.exports.checkEmailExists = (reqBody) => {

	return User.find({email : reqBody.email}).then(result => {


		if (result.length > 0) {

			return true
		}

		return false
	})
}
	

module.exports.registerUser = (reqBody) =>{

	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		mobileNumber: reqBody.mobileNumber,
		email : reqBody.email,	
		password : bcrypt.hashSync(reqBody.password, 10),
		
	})


	return newUser.save().then((user, error) =>{
		if (error){
			return false;
		}else{
			return user;
		}
	})
}




module.exports.login = (reqBody) =>{

	return User.findOne({email : reqBody.email}).then(result =>{

		if(result == null ){
			return {
				message : "Not found in our database"
			}
		}else{
			


			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access : auth.createAccessToken(result)}
			}else{
				return{
					message: "Password incorrect"
				}
			}

		}
	})

}


module.exports.getAllUsers = (data) => {

	// if(data.isAdmin){
	return User.find({}).then(result => {

		return result;

	});

	// };
}



module.exports.toAdmin = (reqParams) =>{

	let updateUser = {
		isAdmin : true
	}

	return User.findByIdAndUpdate(reqParams.userId, updateUser).then((user, error)=>{

		if(error){
			return false
		}else{
			return {
				message : "Updated User successfully"
			}
		}

	})
}

module.exports.toNotAdmin = (reqparams) =>{

	let updateUser = {
		isAdmin : false
	}

	return User.findByIdAndUpdate(reqparams.userId, updateUser).then((user, error)=>{

		if(error){
			return false
		}else{
			return {
				message : "Updated User successfully"
			}
		}

	})
}





module.exports.getDetails = (Id) => {

	return User.findById(Id).then(result => {
		return result;
	})
}

module.exports.getProfile = (userData) => {
	return User.findOne({_id : userData.id}).then(result => {
		if (result == null){
			return false
		} else {
			result.password = ""
			return result
		}
	});
}; 

		

	



module.exports.Checkout = (data) =>{


	return Product.findById(data.product.productId).then(product => {
		if(data.isAdmin){
			let message = Promise.resolve({
				message : 'Admin cannot place order.'
			})


			return message.then((value) =>{
				return value
			})
		}

		let newOrder = new Order({
			userId : data.userId,
			products :[{
				productId: data.product.productId,
				quantity: data.product.quantity
			}],
			totalAmount: product.price * data.product.quantity
		})
		return newOrder.save().then(result =>{
			return Promise.resolve({
				message : "Order successfully created",
				result: result
			})
		})

		let newProduct = new Product({
			Ordered : data.userId
		});

		return newProduct.save().then(result => {
			return Promise.resolve({
				result : result
			})
		});
	})

}


