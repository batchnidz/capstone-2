const express = require("express")
const Product = require("../models/productModel")
const bcrypt = require("bcrypt")
const auth = require("../auth")
const Order = require("../models/Order")





module.exports.registerProduct = (data, file) => {
	if(data.isAdmin){
		let newProduct = new Product ({
				name : data.product.name,	
				description : data.product.description,
				price : data.product.price,
				quantity : data.product.quantity,
				image : file.originalname
		
		})

		return newProduct.save().then((product, error) => {
			if(error){
				return false
			}

			return {
				message : `Added ${data.product.name} succesfully`
			}
		})
	} 

	let message = Promise.resolve('User must me Admin to Access this.')

	return message.then((value) => {
		return value

	
	})
		
}





module.exports.isactivefalse = async (reqParams, data) => {

	if(data.isAdmin) {


	let updateActiveField = {
		isActive : false
	}

	return await Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((status, error)=>{

		if(error){
			return false
		}else{
			return {
				message : "Status updated"
			}
		}

	})
}else{
	return {
		message : "not allowed"
	}
}
}


module.exports.isactivetrue = async (reqParams, data) => {

	if(data.isAdmin) {


	let updateActiveField = {
		isActive : true
	}

	return await Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((status, error)=>{

		if(error){
			return false
		}else{
			return {
				message : "Status updated"
			}
		}

	})
}else{
	return {
		message : "not allowed"
	}
}
}




module.exports.getActive = () => {
	return Product.find({ isActive : true})
}


module.exports.getProdu = (data) => {

	 return Product.findById(data.productId).then(result => {
	 	return result;
	 }) ;
}


module.exports.getProduAdmin = (data) => {

	 return Product.findById(data.productId).then(result => {
	 	return result;
	 }) ;
}





module.exports.getAllProduct = (data) => {

	// if(data.isAdmin){
	return Product.find({}).then(result => {

		return result;

	});

	// };
}


module.exports.getSingleProduct = (Id) => {

	return Product.findById(Id).then(result => {
		return result;
	})

		
	};



module.exports.getOrders = () => {

	return Order.find({ status : "Checkout" }).then(result => {
		return result;
	})
}	


module.exports.updateProduct = async (reqParams, reqBody, data) => {
  if (!data.isAdmin) {
    throw new Error('Unauthorized access');
  }

  const updatedProduct = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
    quantity: reqBody.quantity,
  };

  const result = await Product.findByIdAndUpdate(reqParams.productId, updatedProduct);

  if (!result) {
    throw new Error('Update failed');
  }

  return {
    message: 'Updated successfully',
  };
};
