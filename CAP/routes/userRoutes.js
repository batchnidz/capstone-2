const express = require("express")
const router = express.Router();
const auth = require("../auth");

const userController = require("../controllers/userController")

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result));
});


router.post("/register", (req,res)=>{
	userController.registerUser(req.body).then(result => res.send(result))
})


router.post("/login", (req,res)=>{
	userController.login(req.body).then(result => res.send(result))
});


router.put("/makeAdmin/:userId", auth.verify, (req, res)=>{
	userController.toAdmin(req.params).then(result => res.send(result))
})


router.put("/demoteAdmin/:userId", auth.verify, (req, res)=>{
	userController.toNotAdmin(req.params).then(result => res.send(result))
})



router.post("/createorders", auth.verify, (req,res) => {

	let data = {
		product : req.body,
		userId : auth.decode(req.headers.authorization).id,
		
	}

	userController.Checkout(data).then(result => res.send(result))

})

router.get("/getUserDetails/:id", (req,res) => {


	userController.getDetails(req.params.id).then(result => res.send(result));
})


router.get("/getAllUsers", (req, res) => { 

	userController.getAllUsers().then(result => res.send(result));

});


router.get("/details", auth.verify, (req, res) => {


	const userData = auth.decode(req.headers.authorization)
	console.log(userData)
	console.log(req.headers.authorization)

	userController.getProfile({id : userData.id}).then(resultFromController => res.send(resultFromController))
});















module.exports = router;