const express = require("express");
const router = express.Router();
const multer = require('multer');
const auth = require("../auth");
const productController = require("../controllers/produController")


const storage = multer.diskStorage({
	destination: (req, file, callback) => {
		callback(null, "./uploads")
	},
	filename: (req, file, callback) => {
		callback(null, `${file.originalname}`)
	}
})


const upload = multer({
	storage: storage
});

// Add product
router.post("/addProduct", upload.single("image"), auth.verify, (req,res) => {

	const data = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.registerProduct(data, req.file).then(result => res.send(result))
})

router.put("/update/:id", upload.single('image'), (req, res) =>{
	productController.findById(req.params.id)
	.then((data) => {
		data.name = req.body.name;
		data.description = req.body.description;
		data.price = req.body.price;
		data.quantity = req.body.quantity;
		data.image = req.file.originalname;

		return data.save(); // save the updated data to the database
	})
	.then(() => {
		res.status(200).json({
			message: 'Product updated successfully'
		});
	})
	.catch((err) => {
		res.status(500).json({
			error: err
		});
	});
});


// Edit status
router.put("/archiveProduct/:productId", (req,res) => {

const data = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

		productController.isactivefalse(req.params, data).then(result => res.send(result))
})

// Edit status
router.put("/unarchivedProduct/:productId", (req,res) => {

const data = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

		productController.isactivetrue(req.params, data).then(result => res.send(result))
})




router.get("/getActive", (req, res) => {
	productController.getActive().then(result => res.send(result))

})



router.get("/", (req, res) => { 

	productController.getAllProduct().then(result => res.send(result));

});

router.get("/:productId", (req, res) => {

	productController.getProdu(req.params).then(result => res.send(result));
});


router.get("/Admin/:productId", (req, res) => {

	productController.getProduAdmin(req.params).then(result => res.send(result));
});



router.get("/getOne/:id", (req,res) => {
	


	productController.getSingleProduct(req.params.id).then(result => res.send(result));


})



router.put("/updatedProduct/:productId", auth.verify, (req,res) => {

	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.updateProduct(req.params, req.body, data).then(result => res.send(result))
})




router.get("/getOrders", auth.verify, (req,res)=>{

	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}


	productController.getOrders(data).then(result => res.send(result))
})




/*

Checkout
Get user details
Uploading system to hosting

*/






module.exports = router;