const mongoose = require("mongoose");
mongoose.set('strictQuery', false);

const userSchema =  new mongoose.Schema({

	firstName:{
		type: String,
		required: [true, "firstName required"]
	},
	lastName:{
		type: String,
		required : [true, "lastName required"]
	},
	mobileNumber:{
		type: Number,
		required: [true, "Number is required"]
	},
	email:{
		type: String,
		required : [true, "Email is required"]

	},
	password:{
		type: String,
		required : [true, "Password is required"]
	},
	isAdmin:{
		type: Boolean,
		default : false
	}
	
})


/*userSchema.methods.AddToCart = function(product){
 let cart = this.cart;

 if(cart.items.length == 0){
 	cart.items.push({productId: product._id, quantity: 1})
 }else{
   console.log('User in Schema', this);
   return this.save()
 }
}
*/

module.exports = mongoose.model("User", userSchema);