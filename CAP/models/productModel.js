const mongoose = require("mongoose");
mongoose.set('strictQuery', false);


const productSchema =  new mongoose.Schema({


	name : {
		type: String,
		required: [true, "Name is required"]
	},
	description : {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	quantity: {
		type: Number,
		required: [true, "quantity is required"]
	},
	image:{
		type: String,
		required: [true, "image required"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn: {
		type : Date,
		default: new Date()
	},
	Ordered :[
	{
		userId :{
			type : String,
			required: [true, "userId is required"]
		},
		purchasedOn :{
			type: Date,
			default: new Date()
		}
	}
	] 




})



module.exports = mongoose.model("Product", productSchema);