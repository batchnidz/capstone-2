const mongoose = require("mongoose");
mongoose.set('strictQuery', false);
const Schema = mongoose.Schema;

let ItemSchema = new Schema(
  {
    productId: {
      type: String,
      required: [true, "Product id required"]
    },
    quantity: {
      type: Number,
      required: true,
      min: [true, "Quantity required"],
    },
    totalAmount: {
      type: Number,
      required: true,
    },
  },
  {
    purchaseOn: {
    	type: Date,
    	default: new Date()
    }
  }
);
module.exports = mongoose.model("item", ItemSchema);

const CartSchema = new Schema(
  {
    userId: {
      type: String,
      required : [true, "userId is required"]
    },
    items: [ItemSchema],
    subTotal: {
      default: 0,
      type: Number,
    },
  },
  {
   purchaseOn: {
   	type: Date,
   	default: new Date()
   }
  }
);


module.exports = mongoose.model("cart", CartSchema);