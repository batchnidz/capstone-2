const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const dotenv = require("dotenv").config();

mongoose.set('strictQuery', false);

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes")


const app = express();
const port = 5000;
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.use("/uploads", express.static("./uploads"));


mongoose.connect(`mongodb+srv://nidzzone:${process.env.PASSWORD}@cluster0.u2ylc4z.mongodb.net/Ecom?retryWrites=true&w=majority`,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });


let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.on("open", () => console.log("Connected to MongoDB."));



app.use("/users", userRoutes);
app.use("/products", productRoutes)



app.listen(port, () => console.log(`Api is running on port ${port}`));

